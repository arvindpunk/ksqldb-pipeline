DROP STREAM IF EXISTS godel;

CREATE STREAM godel (
    session_id VARCHAR,
    payment_status VARCHAR,
    merchant_id VARCHAR,
    godel_version VARCHAR,
    order_id VARCHAR,
    system_time VARCHAR,
    browser VARCHAR,
    browser_version VARCHAR,
    os VARCHAR,
    os_version VARCHAR,
    log_version VARCHAR,
    client_id VARCHAR,
    godel_build_version VARCHAR
)
WITH (KAFKA_TOPIC='sessions', VALUE_FORMAT='JSON', PARTITIONS=1);


-- Intermediate topic generation after processing with ksql. (this needs to be changed to actually infer stuff in realtime)
TERMINATE ALL;

DROP TABLE IF EXISTS godel_ksql;

CREATE TABLE godel_ksql
WITH (KAFKA_TOPIC='sessions_ksql', VALUE_FORMAT='JSON') AS 
    SELECT
    merchant_id,
    AS_VALUE(merchant_id) as merchant,
    SUM(CASE WHEN payment_status='SUCCESS' THEN 1 ELSE 0 END) as successful,
    COUNT(*) as total
    FROM godel
    WINDOW TUMBLING (SIZE 10 SECONDS)
    WHERE payment_status IS NOT NULL
    GROUP BY merchant_id
    EMIT FINAL;
